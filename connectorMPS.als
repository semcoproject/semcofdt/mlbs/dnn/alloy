/**
* connectorMPSTick Module 
* 
* Define MPS communication based on Tick in a Component Base Architercture 
*/
module connectorMPSTick

open messaging
open util/ordering[messaging/Tick] as tick
open cbsemetamodel

fact CleanupViz { 
	// Constraint for clean viz (instances must be link to something to exist)
	      all p:Payload | some mp:MsgPassing, t: Tick | p  in PayloadAvailableOnTick[t] or  mp.payload = p
	#MsgPassing >= 1
         #Layer >= 3
       	#buffer.(tick/first) = 0
	#buffer.(tick/last) = 0
}



/*******************************************************************************************************************************
*	Scenario
*******************************************************************************************************************************/

/* Binding our metamodel with the messaging library offering the MPS Behavior */
// A Message is sent 
pred ConnectorMPSSent {
	all n:Neuron, m:MsgPassing, t:Tick |
		 m in t.sentMsg[n] <=> one con:Edge | one t2:t.nexts | n = con.src and m in con.buffer.t2 }

// A Message is in transmission (i.e, into the connector buffer)
pred ConnectorMPSVisible {
	all c:Neuron, m:MsgPassing, t:Tick |
		 m in t.visible[c] <=> one con:Edge | m in con.buffer.t and (con.src = c or con.dest = c) }

// A Message is Received
pred ConnectorMPSReceivedMsg {
	all n:Neuron, m:MsgPassing, t:Tick | 
		 m in t.receivedMsg[n] <=> one con:Edge | con.dest = n and m in con.buffer.t}


fact MPSTraces {
	ConnectorMPSVisible
	ConnectorMPSReceivedMsg
ConnectorMPSSent
}

//run 	{}


/* Communication primitives */
pred Neuron.process[d:Payload, t:Tick]
{
one m:MsgPassing {
		m.payload = d 
		d in t.available[this]
	       }
}

pred Neuron.send[c:Neuron,d:Payload, t:Tick] {
	some m:MsgPassing {
              m.receiver = c
		m.payload = d
		m.sent = t
	}
}

pred Neuron.receive[c:Neuron,d:Payload,t:Tick] {
	some m:MsgPassing {
              m.sender = c
		m.payload = d
		m.received[this]  = t
	       }
}



/*******************************************************************************************************************************
*	Illustrations
*******************************************************************************************************************************/
pred show_send(c1,c2:Neuron, d:Payload, t:Tick) {
		c1.send[c2,d,t]
}

pred show_process(c1:Neuron, d:Payload, t:Tick) {
		c1.process[d,t]
}

pred show_receive(c1,c2:Neuron, d:Payload, t:Tick) {
		c2.receive[c1,d,t]
}

pred show_send_and_receive(t,t2:Tick, c1,c2:Neuron, d:Payload) {
	c1.send[c2,d,t] 
	c2.receive[c1,d,t2]
}

pred show_activate_and_send_and_receive(t0,t1,t2:Tick, c1,c2:Neuron, d:Payload) {
	c1.process[d,t0]
	c1.send[c2,d,t1] 
	c2.receive[c1,d,t2]
}

pred show_2_message_sequence  (c1,c2:Neuron, disj d1,d2:Payload){
	one ts1:Tick {
		c1.send[c2,d1,ts1]
		c1.send[c2,d2,ts1.next]
		c2.receive[c1,d1,ts1.next.next]
		c2.receive[c2,d2,ts1.next.next.next]  
	}
}

pred show_FIFO {
	 all disj c1,c2:Neuron | all disj d1,d2:Payload 
	| all ts1:Tick | let ts2 = ts1.nexts | all tr1:Tick| all tr2:Tick |
	 	(c1.send[c2,d1,ts1]
		and c1.send[c2,d2,ts2]
		and c2.receive[c1,d1,tr1]
		and c2.receive[c1,d2,tr2]) 
		=> tr2 in tr1.nexts
}


//run show_send for 6
//run show_activate for 5
//run show_activate_and_send_and_receive for 5
//run show_receive for 5
//run show_send_and_receive for 5
//run show_2_message_sequence for 8 //doesn't hold
//run show_FIFO for 5

/*******************************************************************************************************************************
*	Verifications
*******************************************************************************************************************************/
// once the neuron n1 activate a payload d eventually this payload will be sent
pred activate_is_eventually_sent{
	one t:Tick | one t2:t.nexts | some c1,c2:Neuron | some d:Payload |
	c1.process[d,t] and are_connected[c1,c2] =>  c1.send[c2,d,t2]
}

// once the client c1 sends a message to server c2 eventually that server receives it
pred send_is_eventually_received{
	one t:Tick | one t2:t.nexts | some c1,c2:Neuron | some d:Payload |
	c1.send[c2,d,t] => c2.receive[c1,d,t2]
}

// once the server c2 receives a message, it must already have been sent by a certain client c1
assert recieve_must_be_sent{	
	all t:Tick | all t2:t.nexts | some c1,c2:Neuron | some d:Payload |
	c2.receive[c1,d,t2] => c1.send[c2,d,t] 
}

// once the server c2 receives a message, it must already have been sent by a certain client c1
assert send_must_be_activated{	
	all t:Tick | all t2:t.nexts | some c1,c2:Neuron | some d:Payload |
         c1.send[c2,d,t2] => c1.process[d,t] 
}

// messages sent from the Neuron c1 to the Neuron c2 reach the c2 in the same order
// as they were sent from c1
assert is_FIFO {
	 all disj c1,c2:Neuron | all disj d1,d2:Payload | all rs1:Tick | all rs2:rs1.nexts
 	| all rr1:Tick | all rr2:Tick |
		(c1.send[c2,d1,rs1]
		and c1.send[c2,d2,rs2]
		and c2.receive[c1,d1,rr1]
		and c2.receive[c1,d2,rr2]) 
		=> rr2 in rr1.nexts
}

pred message_can_be_received_by_other {
	 some disj c1,c2,c3:Neuron| some d:Payload| one t:Tick, t0:t.prevs |
		c1.send[c2,d,t0] and c3.receive[c1,d,t]
}

pred message_can_be_lost {
	  all disj c1,c2,c3:Neuron| some d:Payload| one t:Tick, t0:t.prevs |
		c1.send[c2,d,t0] and not c3.receive[c1,d,t]
}

//run activate_is_eventually_sent for 5
//run send_is_eventually_received for 5
//check recieve_must_be_sent for 5  
//check send_must_be_activated for 5
//check is_FIFO for 4 but 10 Tick, exactly 1 Edge, exactly 1 Weight  
//run message_can_be_received_by_other for 5 
//run message_can_be_lost for 2 but 5 Tick 


run {} for 6 but exactly 1 InputNeuron, exactly 1 HiddenNeuron, exactly 0 BiasNeuron,
 exactly 1 OutputNeuron, exactly 2 Edge

