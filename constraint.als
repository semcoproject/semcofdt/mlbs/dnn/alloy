module constraint

open cbsemetamodel
open messaging
open connectorMPS
open util/ordering[messaging/Tick] as tick

// Extends MsgPassing with get_pld, get_rcv, get_src actions

sig MsgPassingConstraint extends MsgPassing {
	set_pld: Neuron -> Tick ,
	set_rcv: Neuron -> Tick,
	set_src: Neuron -> Tick,
	get_pld: Neuron ->Tick,
	get_rcv: Neuron ->Tick,
	get_src: Neuron ->Tick
}{
	all c:Neuron, t:Tick | get_src.t = c => some m:MsgPassingConstraint, s:Neuron | get_src[c,m,s,t]
	all c:Neuron, t:Tick | get_rcv.t = c => some m:MsgPassingConstraint, r:Neuron | get_rcv[c,m,r,t]
	all c:Neuron, t:Tick | get_pld.t = c => some m:MsgPassingConstraint, d:Payload | get_pld[c,m,d,t]
	all c:Neuron, t:Tick | set_src.t = c => some m:MsgPassingConstraint, s:Neuron | set_src[c,m,s,t]
	all c:Neuron, t:Tick | set_rcv.t = c => some m:MsgPassingConstraint, r:Neuron | set_rcv[c,m,r,t]
	all c:Neuron, t:Tick | set_pld.t = c => some m:MsgPassingConstraint, d:Payload | set_pld[c,m,d,t]  
}

sig EdgeConstraint extends Edge {
	get_wet: Neuron -> Tick ,
	set_wet: Neuron -> Tick,
}
{
	all c:Neuron, t:Tick | get_wet.t = c => some e:EdgeConstraint, w:Weight | get_wet[c,e,w,t]
	all c:Neuron, t:Tick | set_wet.t = c => some e:EdgeConstraint, w:Weight | set_wet[c,e,w,t]  
}

/**
Axioms
*/
fact axioms { // add axiome intercept ==> process
	all m:MsgPassingConstraint, c1:Neuron, t:Tick|   // intercept ==> it was injected
		intercept[c1,m, t] => some c2:Neuron | injected[c2,m, t]
	
	all m:MsgPassingConstraint, c1:Neuron, t1:Tick |  //inject ==>  src was set previously to some neuron c2
		inject[c1,m,t1] => some c2:Neuron, t0:t1.prevs | set_src[c1,m,c2,t0]

	all m:MsgPassingConstraint, c1:Neuron, t1:Tick |  //inject ==>  the receiver was set previously to c2
		inject[c1,m,t1] =>  some c2:Neuron, t0:t1.prevs| set_rcv[c1,m,c2,t0]

	all m:MsgPassingConstraint, c:Neuron, t1:Tick |  //inject ==> the neuron has activated the payload and it  was set
		inject[c,m,t1] =>  some d:Payload, t0:t1.prevs | process[c,d,t0] and  set_pld[c,m,d,t0]

	all m:MsgPassingConstraint, c,s:Neuron, t:Tick |  //c get_src s from m ==> m intercepted by c and m.sender = s 
		get_src[c,m,s,t] => intercepted[c,m,t] and m.sender = s

	all m:MsgPassingConstraint, c,r:Neuron, t:Tick | // c get_rcv r from m ==> m intercepted by c and m.receiver = s 
		get_rcv[c,m,r,t] =>  intercepted[c,m, t] and m.receiver = r

	all m:MsgPassingConstraint, c:Neuron, d:Payload, t:Tick | //c get_pld d from m ==> m intercepted by c and m.payload = d 
		get_pld[c,m,d,t] =>  intercepted[c,m, t] and m.payload = d
}

/** E Modalities */ 
// Eprocess: a c Neuron have the opportunity to process msgs at a Tick t if it has received all msgs
pred E_activate[n: Neuron, t:Tick]
{
	n.layer = t.active
some m:MsgPassingConstraint |  intercepted[n,m,t] 
}


// EInject : a c Neuron have the opportunity to inject a message m at a Tick t
pred E_inject[n:Neuron,m:MsgPassingConstraint, t:Tick] {
	n.layer = t.active
	some con:Edge | con.src = n 
}

// EIntercept : a c Neuron have the opportunity to intercept a message m at a Tick t
pred E_intercept[n:Neuron,m:MsgPassingConstraint, t:Tick] {
	n.layer = t.active
	some con:Edge | con.dest = n and m in con.buffer.t
}


// Esetrcv: a c Neuron have the opportunity to set the declared source of a message m at a Tick t
pred E_set_rcv[c:Neuron, m:MsgPassingConstraint, r:Neuron, t:Tick] {
	not injected[c,m,t]
}

// Esetpld : a c Neuron have the opportunity to set the payload of a message m from a declared source sat a Tick t
pred E_set_pld[c:Neuron, m:MsgPassingConstraint, d:Payload, t:Tick] {
	not injected[c,m,t]
}

// Esetsrc : a c Neuron have the opportunity to set the source of a message m at a Tick t
pred E_set_src[c:Neuron, m:MsgPassingConstraint, s:Neuron, t:Tick] {
	not injected[c,m,t]
}

// Egetsrc : a c Neuron have the opportunity to get the source of a message m at a Tick t
pred E_get_src[c:Neuron, m:MsgPassingConstraint, s:Neuron, t:Tick] {
	intercepted[c,m,t]
	m.sender = s
}

// Egetrcv: a c Neuron have the opportunity to get the declared source of a message m at a Tick t
pred E_get_rcv[c:Neuron, m:MsgPassingConstraint, r:Neuron, t:Tick] {
	intercepted[c,m,t]
	m.receiver = r
}

// Egetpld : a c Neuron have the opportunity to get the payload of a message m from a declared source sat a Tick t
pred E_get_pld[c:Neuron, m:MsgPassingConstraint, d:Payload, t:Tick] {
	intercepted[c,m,t]
	m.payload = d
}

// Egetwet a c Neuron have the opportunity to get the declared weight of a edge e at a Tick t
pred E_get_wet[c:Neuron, e:EdgeConstraint, w:Weight, t:Tick] {
some m:MsgPassing | m.sender = e.src and m.receiver = e.dest and m in t.visible[c] 	
e.weight = w
}

// Esetwet a c Neuron have the opportunity to set the weight of a edge e at a Tick t
pred E_set_wet[c:Neuron, e:EdgeConstraint, t:Tick] {
no e.buffer.t
e.dest = c or e.src = c
}

/** 
	Actions 
*/
// inject : a Neuron c added a message m into the system at a tick  t
pred inject[c1:Neuron,m:MsgPassingConstraint, t:Tick] {
	E_inject[c1,m,t]
	some c2:Neuron | are_connected[c1,c2] and c1.send[c2,m.payload,t]
}

// activate: a Neuron n processes a message m into the system at a tick  t
pred activate[c1:Neuron, p:Payload, t:Tick] {
	E_activate[c1,t]
	c1.process[p,t]
}

// intercept: a Neuron got the message m from the system at a tick t
pred intercept[c1:Neuron,m:MsgPassingConstraint, t:Tick] {
	E_intercept[c1,m,t]
some c2:Neuron | are_connected[c1,c2] and c1.receive[c2,m.payload,t] // faux not the same payload to send
}

// get_wet: a Neuron got the  declared weight w from the edge e at a tick t
pred get_wet[c:Neuron,e:EdgeConstraint, w:Weight, t:Tick] {
	E_get_wet[c,e,w,t]
	e.get_wet.t = c
}

// set_wet : a Neuron set the declared weight w to the edge e at a tick t
pred set_wet[c:Neuron,e:EdgeConstraint, w:Weight, t:Tick] {
	E_set_wet[c,e,t]
	e.set_wet.t = c
       e.weight = w 
}

// set_src : a Neuron set the  declared source s from the message m at a tick t
pred set_src[c:Neuron,m:MsgPassingConstraint, s:Neuron, t:Tick] {
	E_set_src[c,m,s,t]
	m.set_src.t = c
	m.sender = s 
}

//  set_rcv : a Neuron set  the receiver(s) r from the message m at a tick t
pred set_rcv[c:Neuron, m:MsgPassingConstraint, r:Neuron, t:Tick] {
	E_set_rcv[c,m,r,t]
	m.set_rcv.t = c
	m.receiver = r
}

//  set_pld : a Neuron set the payload d from the message m at a tick t
pred set_pld[c:Neuron, m:MsgPassingConstraint, d:Payload, t:Tick] {
	E_set_pld[c,m,d,t]
	m.set_pld.t = c
	m.payload = d
}

// get_src : a Neuron got the  declared source s from the message m at a tick t
pred get_src[c:Neuron,m:MsgPassingConstraint, s:Neuron, t:Tick] {
	E_get_src[c,m,s,t]
	m.get_src.t = c
}

//  get_rcv : a Neuron got the receiver(s) r from the message m at a tick t
pred get_rcv[c:Neuron, m:MsgPassingConstraint, r:Neuron, t:Tick] {
	E_get_rcv[c,m,r,t]
	m.get_rcv.t = c
}

//  get_pld : a Neuron got the payload d from the message m at a tick t
pred get_pld[c:Neuron, m:MsgPassingConstraint, d:Payload, t:Tick] {
	E_get_pld[c,m,d,t]
	m.get_pld.t = c
}

/** Macros */
// injected : a Neuron c added a message m into the system before a tick  t
pred injected[c:Neuron,m:MsgPassingConstraint, t1:Tick] {
	one t2:t1.prevs | inject[c,m,t2]
}

// intercepted : a Neuron got the message m from the system before a tick t
pred intercepted[c:Neuron,m:MsgPassingConstraint, t1:Tick] {
	one t2:t1.prevs | intercept[c,m,t2]
}

// sent_by : a c Neuron injected a message m and c is the accurate origin of m m.payload
pred sent_by[m:MsgPassingConstraint,c:Neuron, t:Tick] {
	injected[c,m,t] and m.sender = c
}

// sent_with : a Neuron c injected a message m that contains a paylaod d
pred sent_with[m:MsgPassingConstraint, d:Payload, t:Tick] {
	one c:Neuron | sent_by[m,c,t] and m.sender = c and m.payload = d 
}

// sent_to : some c Neuron injected a message m with r has intended receivers
pred sent_to[m:MsgPassingConstraint,r:set Neuron, t:Tick] {
	one c:Neuron | sent_by[m,c,t] and r = m.receiver
}

run {} for 3 but exactly 1 InputNeuron, exactly 1 HiddenNeuron, exactly 0 BiasNeuron,
exactly 1 OutputNeuron, exactly 2 Edge,  8 Tick
