/**
* Property Module
*
* Property abstraction  for 
*/
module stride

open cbsemetamodel
open connectorMPS


// Abstract property on neuron
abstract sig NeuronProperty {
	neur: one Neuron,
}

fun NeuronProperty.holds[n:one Neuron]: set NeuronProperty {
	{ prop:this {	 prop.neur = n  }}
}

// Abstract property on edge
abstract sig EdgeProperty {
	edg: one Edge,
}

fun EdgeProperty.holds[e:one Edge]: set EdgeProperty {
	{ prop:this { prop.edg = e}}
}

