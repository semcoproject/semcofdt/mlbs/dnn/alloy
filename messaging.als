module messaging
open util/ordering[Tick] as ord
open util/ordering[Layer] as lay


abstract sig Layer {
}

abstract sig Neuron {
layer: one Layer
}

sig Payload {
}

abstract sig Msg {
   // Node that sent the message
   sender: one Neuron,
   // Intended recipient(s) of a message; note that broadcasts are allowed
   receiver: one Neuron,
   // Timestamp: the tick on which the message was sent
   sent: one Tick,
   // tick at which node reads message, if read
   received: Neuron -> lone Tick,
   payload: one Payload
}

sig Tick {
     active : one Layer,
     available: Neuron -> one Payload,
     sentMsg: Neuron -> Msg, 
     visible: Neuron ->  Msg, 
     receivedMsg: Neuron -> Msg 
  }

fun PayloadAvailableOnTick[t:Tick]: set Payload{ t.available[Neuron]}
fun MsgsSentOnTick[r: Tick]: set Msg { r.sentMsg[Neuron] }
fun MsgsVisibleOnTick[r: Tick]: set Msg { r.visible[Neuron] }
fun MsgsReceivedOnTick[r: Tick]: set Msg { r.receivedMsg[Neuron] }

fact MsgMovementConstraints {
   // At the beginning, no messages have been sent yet, nor received, nor visible
   ord/first.active = lay/first 
   ord/last.active = lay/last 
   all pre: Tick - ord/last |
     let post = ord/next[pre] | {
        // messages sent on this tick are no longer available on subsequent tick
        post.available[Neuron] = pre.available[Neuron] - MsgsSentOnTick[pre].payload
     }

   some p:Payload | p in ord/first.available[Neuron]
	   no ord/first.visible[Neuron]
   all t: Tick | {
      MsgsSentOnTick[t].sent in t
      MsgsReceivedOnTick[t].received[Neuron] in t
      all e: Neuron, m: Msg | {
			           //m.sent = t => m in t.sentMsg[e]
           m.received[e] = t => m in t.receivedMsg[e]  // m is signed as received by e at t implies that m is in the list of received msg at t
/* We don't have: m in t.receivedMsg[e] => m.received[e] = t  bkz 
we can have messages declared received at t by e however the message is not signed as received.  */
		            }
      all e: Neuron | t.sentMsg[e].sender in e
      all e: Neuron, m: Msg| {
			     	          // payload p is in message sent at tick t by neuron e  implies that p was activated previously
	  (m in t.sentMsg[e] => m.payload in t.available[e] and m.payload !in ord/nexts[t].available[e]) 
                 // message m is visible for neuron e at tick t implies that m was sent by n previously 
          (m in t.visible[e] => (m.sent in ord/prevs[t]) ) 
                 // message m is received by neuron e at tick t implies that m is visible to e at t and will not be visible after.   
          (m in t.receivedMsg[e] => m in t.visible[e] and m !in ord/nexts[t].visible[e])
      }
	// There is almost one msg that is received by neuron e at t iff m is not sent by neuron e at t
	  lone e: Neuron, m: Msg  | m in t.receivedMsg[e] iff not m in t.sentMsg[e]
   }
}

fact CleanupViz {
    // all messages must be sent
    Msg in Tick.sentMsg[Neuron]
}

run { }
