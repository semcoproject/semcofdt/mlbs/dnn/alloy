open constraint
open property

sig WeightConfidentiality extends EdgeProperty { } {
WeightConfidentiality[edg]
}
pred weightConfidentiality[e:Edge] { 
	 all w:Weight, t:Tick, disjoint n,nd: Neuron |
               e.dest = nd and e.weight = w => not E_get_wet[n,e,w,t]
}
assert confidentialityNotHold {
	all e: Edge|  WeightConfidentiality.holds[e]
}

/*************************/
sig BiasNeuronIntegrity extends NeuronProperty { } {
biasNeuronIntegrity[neur]
}
pred biasNeuronIntegrity[n: BiasNeuron]  {
        all b:Bias, m: MsgPassingConstraint, n2:Neuron,  t:Tick |
	    E_get_pld[n2,m,b,t] and sent_by[m,n,t] implies sent_with[m,b,t]
}
assert integrityNotHold {
	all bn: BiasNeuron |  BiasNeuronIntegrity.holds[bn] 
}

/***************************/
sig EdgeAvailability extends EdgeProperty { } {
EdgeAvailability[edg]
}
pred edgeAvailability [e:Edge] {
     	all m: MsgPassing, p:Payload, t:Tick |    let nd = e.dest , ns = e.src | 
            inject [ns,m,t] and m.receiver = nd  and m.payload = p   => intercept[nd,m,t]
}
assert availabilityNotHold {
	all e:Edge |  EdgeAvailability.holds[e] 
}
