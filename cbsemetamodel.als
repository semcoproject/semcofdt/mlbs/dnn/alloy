/**
* CBSEMetamodel Module 
* 
* Define an Alloy Meta model for Component Base Architercture
*/
module cbsemetamodelTick
open util/ordering[Tick] as tick
open util/ordering[Layer]
open messaging


/*******************************************************************************************************************************
*	Logical
*******************************************************************************************************************************/


sig Bias extends Payload{
}

sig Weight {
}

sig Edge {
src: one Neuron, 
dest: one Neuron,
buffer : set  MsgPassing -> Tick,
weight: one Weight 
}

sig MsgPassing extends Msg{

}

sig InputNeuron extends Neuron{
}
sig OutputNeuron extends Neuron {
}
sig BiasNeuron extends Neuron {
bias : one Bias
}
sig HiddenNeuron extends Neuron {
}

fact  { 
#OutputNeuron >= 1
#InputNeuron >= 1
// Repartition of neurons per layer 
all n:InputNeuron | n.layer = first // inputNeurons are in the first layer
all n:Neuron | n.layer = first implies n in InputNeuron  
all n:Neuron | n.layer = first implies (n in InputNeuron or n in BiasNeuron)  // first layer contains only Input or Bias Neurons 
all n:OutputNeuron | n.layer = last  // outputNeurons are in the last layer
all n:Neuron | n.layer = last implies n in OutputNeuron //  // last layer contains only OutputNeurons 
all l:Layer - first - last | lone bn:BiasNeuron  |  bn.layer = l  // 
lone bn:BiasNeuron |  bn.layer = first  // Bias Neuron might be in the first layer
all l:Layer - first - last | some hn:HiddenNeuron | hn.layer = l    //  Each hidden layer contains some neurons


// Edges
all e:Edge | e.dest.layer = e.src.layer.next
no disj e1, e2 : Edge | e1.src = e2.src and e1.dest = e2.dest
all n:Neuron, hn:HiddenNeuron | hn.layer = n.layer.next implies one e:Edge | e.src = n and e.dest = hn  // fully connected to hidden Neuron
all n:Neuron, on:OutputNeuron | on.layer = n.layer.next implies one e:Edge | e.src = n and e.dest = on  // fully connected to output Neuron
all e:Edge | not (e.dest in BiasNeuron) // bias neuron are not destination


// only connected atoms
all w:Weight | some e:Edge | e.weight = w
all b:Bias | some n: BiasNeuron | n.bias = b
all l: Layer | some n:Neuron | n.layer = l
 
}

*/

pred are_connected[n1:Neuron, n2:Neuron] {
one e:Edge | e.src = n1 and e.dest = n2
}



//run {} for 3  but exactly 3 InputNeuron, exactly 4 HiddenNeuron, exactly 2 BiasNeuron,
//exactly 2 OutputNeuron, exactly 18 Edge, exactly 4 Layer


